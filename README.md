# spdlog

Header only, C++ logging library.
https://github.com/gabime/spdlog

## Documentation
Documentation can be found in the [wiki](https://github.com/gabime/spdlog/wiki/1.-QuickStart) pages.

## Install
Just copy the source https://github.com/gabime/spdlog/tree/master/include/spdlog to your build tree and use a C++11 compiler
